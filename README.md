# oblivoir patch

## 고지사항

* `oblivoir-patch3.sty`는 TeX Live 2022에 탑재된 oblivoir 3.1에 대한 패치입니다.
* `oblivoir-patch2.sty`는 TeX Live 2021에 탑재된 oblivoir 3.0.1에 대한 패치입니다. 이 패치의 내용은 oblivoir version 3.1에 반영되었으며 더이상 업데이트하지 않습니다.
* `oblivoir-patch1` v4까지의 patch는 2021/03/03, oblivoir v3.0에 반영되었습니다. oblivoir-patch1.sty의 개발은 이로써 종료합니다. 이 파일은 compatibility를 위하여 유지합니다. 새 버전의 oblivoir로 작성하는 문서에서 oblivoir-patch1.sty를 로드하여도 아무런 작용을 하지 않습니다.

## 안내

CTAN과 TEXLIVE의 oblivoir 클래스군을 업데이트하기 위한 준비로서 누적된 패치들을 한 개의 (임시) style 파일로써 제공합니다.

## 주의

* 예고없이 내용이 변경될 수 있습니다. 
* CTAN의 oblivoir가 업데이트되면 내용이 삭제될 수 있습니다.
* 이 패키지에 대해서 아무런 보증도 하지 않습니다. 자신의 책임으로 사용하되, 문제가 발생하면 [KTUG의 질/답 게시판](http://ktug.org)에 알려주십시오.

## 사용법

### 다운로드

```bash
git clone https://bitbucket.org/novadh/oblivoir-patch
```

### 설치

* $TEXMFHOME(보통 `~/texmf` 또는 `~/Library/texmf`) 아래 다음 위치에 가져다둡니다.

```
$TEXMFHOME/tex/latex/oblivoir-patch/oblivoir-patch3.sty
```

### 문서에서 사용법

```latex
\documentclass{oblivoir}
\usepackage{oblivoir-patch3}
```

## 변경사항

### 2022/03/06, patch v1

1. fapapersize의 \selectfageometry 기능 개선. trimmark 여백을 별도로 계산해야 하던 문제를 수정함.